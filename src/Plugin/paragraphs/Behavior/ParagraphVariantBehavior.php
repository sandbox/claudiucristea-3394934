<?php

declare(strict_types = 1);

namespace Drupal\paragraph_variant\Plugin\paragraphs\Behavior;

use Drupal\cl_editorial\Form\ComponentInputToForm;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\sdc\ComponentPluginManager;
use SchemaForms\FormGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a behavior for paragraph variants.
 *
 * @ParagraphsBehavior(
 *   id = "paragraph_variant",
 *   label = @Translation("Paragraph variants"),
 *   description = @Translation("Allows selecting a variant (which is a SDC) and renders the paragraph using that variant"),
 * )
 */
class ParagraphVariantBehavior extends ParagraphsBehaviorBase {

  use AjaxHelperTrait;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityFieldManagerInterface $entity_field_manager,
    protected readonly ComponentPluginManager $componentManager,
    protected readonly FormGeneratorInterface $formGenerator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.sdc'),
      $container->get('cl_editorial.form_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'variants' => [],
      'enforce' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode): void {

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['variants'] = [
      '#type' => 'select',
      '#title' => $this->t('Variants (SDC)'),
      '#description' => $this->t('Select one or more Single Directory Components to be vailable in the paragrapg type behavior form'),
      '#multiple' => TRUE,
      '#options' => $this->getVariants(),
      '#default_value' => $this->getConfiguration()['variants'],
    ];
    $form['enforce'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force users to select a predefined option'),
      '#default_value' => $this->getConfiguration()['enforce'],
      '#description' => $this->t('If checked, then choosing a predefined variant is mandatory.'),
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration([
      'variants' => array_values($form_state->getValue('variants')),
      'enforce' => (bool) $form_state->getValue('enforce'),
    ] + $this->getConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): void {
    $variant = $paragraph->getBehaviorSetting('paragraph_variant', 'variant');
    $settings = $paragraph->getBehaviorSetting('paragraph_variant', 'settings', []);
    $form['variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Variant to use'),
      '#options' => $this->getVariants($this->getConfiguration()['variants']),
      '#required' => $this->getConfiguration()['enforce'],
      '#empty_value' => NULL,
      '#empty_option' => $this->getConfiguration()['enforce'] ? $this->t('- Select -') : $this->t('Default'),
      '#default_value' => $variant,
      '#ajax' => ['callback' => [$this, 'ajaxSubmit1']],
    ];

    if ($this->isAjax()) {
      // @see https://www.drupal.org/node/2897377
      $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    }

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    if ($variant) {
      $componentToForm = new ComponentInputToForm($this->componentManager, $this->formGenerator);
      $subFormState = SubformState::createForSubform($form['settings'], $form, $form_state);
      $form['settings'] = $componentToForm->buildForm($variant, $settings, $form['settings'], $subFormState);
    }
  }

  public function validateBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    parent::validateBehaviorForm($paragraph, $form, $form_state);
  }

  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    parent::submitBehaviorForm($paragraph, $form, $form_state);
  }

  protected function getVariants(?array $limitTo = NULL): array {
    $definitions = $this->componentManager->getDefinitions();
    $variants = array_map(fn(array $definition) => $definition['name'], $definitions);
    if ($limitTo !== NULL) {
      $variants = array_intersect_key($variants, array_flip($limitTo));
    }
    return $variants;
  }

  public function ajaxSubmit1(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -1000,
      ];
      $form['#sorted'] = FALSE;
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]', $form));
    }
    else {
      $response = $this->successfulAjaxSubmit($form, $form_state);
    }
    return $response;
  }

  protected function successfulAjaxSubmit(
    array $form,
    FormStateInterface $form_state
  ) {
    $selector = '[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]';
    return (new AjaxResponse())->addCommand(new ReplaceCommand($selector, $form));
  }

}
